package com.badlynx.spaceapps;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.IntAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;

public class MyGdxGame extends ApplicationAdapter {

    PerspectiveCamera camaraPerspectiva;
    CameraInputController camaraInput;
    ModelInstance esfera;
    ModelInstance espacio;
    ModelInstance luna;
    Environment luces;
    ModelBatch modelBatch;
    AssetManager assetManager = new AssetManager();
	RejillaDebug rejillaDebug;
    Stage stage;
    Interfaz interfaz;
    InputMultiplexer multiplexer;

    public final static float DIAMETRO_TIERRA = 12742f;
    public final static float DIAMETRO_LUNA = 3474f;
    public final static float DISTANCIA_TIERRA_LUNA = 370300 / 6f;
    float escala = 1 / 3474f;

	@Override
	public void create () {

        // CREAMOS LA INTERFAZ
        stage = new Stage(new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
        interfaz = new Interfaz();
        interfaz.iniciar(stage, assetManager);

//        rejillaDebug = new RejillaDebug(20, 20);

        // CREAMOS LA CAMARA
        modelBatch = new ModelBatch();
        camaraPerspectiva = new PerspectiveCamera(67, Graficos.getX(1), Graficos.getY(1));
        float distancia = 0.3f;
        camaraPerspectiva.position.set(DIAMETRO_LUNA * escala * 1.01f, DIAMETRO_LUNA * escala * 1.01f, DISTANCIA_TIERRA_LUNA * escala * 0.7f);
        camaraPerspectiva.lookAt(0, 0, 0);
        camaraPerspectiva.near = 1;
        camaraPerspectiva.far = 170;                // DISTANCIA MAXIMA QUE VEMOS (RENDIMIENTO)
        camaraPerspectiva.fieldOfView = 50;
        camaraPerspectiva.translate(camaraPerspectiva.position.scl(1));
        camaraPerspectiva.update();

        // CREAMOS EL INPUT DE LA CAMARA
        camaraInput = new CameraInputController(camaraPerspectiva);
        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(camaraInput);
        multiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(multiplexer);

        // CREAMOS LAS LUCES
        luces = new Environment();
        luces.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 0.5f));
        DirectionalLight directionalLight = new DirectionalLight();
        directionalLight.set(0.8f, 0.8f, 0.8f, new Vector3(-1, -0.8f, -0.2f));
        luces.add(directionalLight);

        // CREAMOS EL ESPACIO Y LA TIERRA CON MESHES A VER QUE TAL
//        assetManager.load("earth.png", Texture.class);
        assetManager.load("earth2.jpg", Texture.class);
        assetManager.load("space.jpg", Texture.class);
        assetManager.load("moon.jpg", Texture.class);
        assetManager.finishLoading();

        // CARGAMOS LAS TEXTURAS
//        Texture texture = assetManager.get("earth.png", Texture.class);
        Texture texture = assetManager.get("earth2.jpg", Texture.class);
        Texture textureSpace = assetManager.get("space.jpg", Texture.class);
        Texture textureMoon = assetManager.get("moon.jpg", Texture.class);
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        textureSpace.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        // CREAMOS LOS MATERIALES
        Material materialEarth = new Material(TextureAttribute.createDiffuse(texture));
        Material materialSpace = new Material(TextureAttribute.createDiffuse(textureSpace));
        Material materialMoon = new Material(TextureAttribute.createDiffuse(textureMoon));

        // UTILIZAMOS LOS MATERIALES PARA CREAR LOS MODELOS 3D
        ModelBuilder constructor = new ModelBuilder();
        Model esferaModel = constructor.createSphere(DIAMETRO_TIERRA * escala, DIAMETRO_TIERRA * escala, DIAMETRO_TIERRA * escala, 100, 100, materialEarth, VertexAttributes.Usage.Position | VertexAttributes.Usage.TextureCoordinates | VertexAttributes.Usage.Normal);
        Model spaceModel = constructor.createSphere(150, 150, 150, 100, 100, materialSpace, VertexAttributes.Usage.Position | VertexAttributes.Usage.TextureCoordinates | VertexAttributes.Usage.Normal);
        Model moonModel = constructor.createSphere(DIAMETRO_LUNA * escala, DIAMETRO_LUNA * escala, DIAMETRO_LUNA * escala, 100, 100, materialMoon, VertexAttributes.Usage.Position | VertexAttributes.Usage.TextureCoordinates | VertexAttributes.Usage.Normal);
        esfera = new ModelInstance(esferaModel);
        espacio = new ModelInstance(spaceModel);
        luna = new ModelInstance(moonModel);
        luna.transform.setTranslation(new Vector3(0, 0, DISTANCIA_TIERRA_LUNA * escala));

        // MARCAMOS EL MODELO DE ESPACIO DEL TIPO BOVEDA
        espacio.materials.get(0).set(new IntAttribute(IntAttribute.CullFace, 0));
	}

	@Override
	public void render () {
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        modelBatch.begin(camaraPerspectiva);
        modelBatch.render(esfera, luces);
        modelBatch.render(luna, luces);
        modelBatch.render(espacio);
//        rejillaDebug.dibujar(modelBatch);
        modelBatch.end();

        esfera.transform.rotate(0, 1, 0, 0.1f);

//        camaraPerspectiva.direction.set(x, y, z);
//        camaraPerspectiva.update();

        // PINTAMOS LA INTERFAZ
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();

	}
}


