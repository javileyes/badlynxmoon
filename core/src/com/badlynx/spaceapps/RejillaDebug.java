package com.badlynx.spaceapps;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;

public class RejillaDebug {

    final float MINIMO_REJILLA = -10f;
    final float MAXIMO_REJILLA = 10f;
    final float PASO_REJILLA = 1f;

    Model ejesModel;
    ModelInstance ejes;

    public RejillaDebug(float w, float h) {
        ModelBuilder constructorModel = new ModelBuilder();
        constructorModel.begin();
        MeshPartBuilder constructor = constructorModel.part("rejilla", GL20.GL_LINES, VertexAttributes.Usage.Position | VertexAttributes.Usage.ColorUnpacked, new Material());
        constructor.setColor(Color.LIGHT_GRAY);
        for (float t = MINIMO_REJILLA; t <= MAXIMO_REJILLA; t += PASO_REJILLA) {
            constructor.line(t, 0, MINIMO_REJILLA, t, 0, MAXIMO_REJILLA);
            constructor.line(MINIMO_REJILLA, 0, t, MAXIMO_REJILLA, 0, t);
        }
        constructor = constructorModel.part("ejes", GL20.GL_LINES, VertexAttributes.Usage.Position | VertexAttributes.Usage.ColorUnpacked, new Material());
        constructor.setColor(Color.RED);
        constructor.line(0, 0, 0, h * 4, 0, 0);
        constructor.setColor(Color.GREEN);
        constructor.line(0, 0, 0, 0, 100, 0);
        constructor.setColor(Color.BLUE);
        constructor.line(0, 0, 0, 0, 0, w * 4);
        ejesModel = constructorModel.end();
        ejes = new ModelInstance(ejesModel);
    }

    public void dibujar(ModelBatch modelBatch) {
        modelBatch.render(ejes);
    }

}
