package com.badlynx.spaceapps;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;

public class Interfaz {

    Label titulo;
    Label diametroTierra;
    Label diametroLuna;
    Label distanciaTierraLuna;
    Stage stage;
    AssetManager assetManager;


    public Interfaz() {

    }

    public void iniciar(Stage stage, AssetManager assetManager) {
        this.assetManager = assetManager;
        this.stage = stage;

        BitmapFont font;
        Skin skin = new Skin(Gdx.files.internal("scene2d/uiskin.json"));
//        font = skin.getFont("default-font");
        font = cargarFont("scene2d/HAMMERHEAD.ttf", (int) Graficos.getX(0.02f));

//        font.getData().scale(1);
//        font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        Table tabla = new Table(skin);
        Label.LabelStyle style = new Label.LabelStyle(font, Color.GREEN);
        titulo = new Label("La Luna sobre la Tierra", style);
        titulo.setAlignment(Align.left);
        tabla.add(titulo);
        tabla.row();

        diametroTierra = new Label("Diametro de la Tierra: " + MyGdxGame.DIAMETRO_TIERRA + " km", style);
        tabla.add(diametroTierra);
        tabla.row();

        diametroLuna = new Label("Diametro de la Luna: " + MyGdxGame.DIAMETRO_LUNA+ " km", style);
        tabla.add(diametroLuna);
        tabla.row();

        distanciaTierraLuna = new Label("Distancia entre las dos: " + MyGdxGame.DISTANCIA_TIERRA_LUNA + " km", style);
        tabla.add(distanciaTierraLuna);
        tabla.row();

        stage.addActor(tabla);
        tabla.setPosition(Graficos.getX(0.5f), Graficos.getY(0.15f));
    }

    public BitmapFont cargarFont(String fontPath, int size) {
        FileHandleResolver resolver = new InternalFileHandleResolver();
        assetManager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
        assetManager.setLoader(BitmapFont.class, ".ttf", new FreetypeFontLoader(resolver));

        FreetypeFontLoader.FreeTypeFontLoaderParameter parametros = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        parametros.fontFileName = fontPath;
        parametros.fontParameters.size = size;
        assetManager.load("font" + size + ".ttf", BitmapFont.class, parametros);
        assetManager.finishLoading();
        return assetManager.get("font" + size + ".ttf", BitmapFont.class);
    }

}


//    Stage stage;
//    BitmapFont font;
//
//    public void iniciar() {
//        if (Ajustes.ajustesEspecificos.GAME().getScreen() instanceof PantallaJuego) {
//            stage = Escenario.getInstancia();
//        } else {
//            stage = ((Pantalla) Ajustes.ajustesEspecificos.GAME().getScreen()).interfaz.getEscenario();
//        }
//        Skin skin = new Skin(Gdx.files.internal("Escenario/uiskin.json"));
////		font = cargarFont(Ajustes.ajustesEspecificos.ASSETS + "testmode/CabinSketch-regular.otf", 80);
////		font = cargarFont(Ajustes.ajustesEspecificos.ASSETS + "testmode/belligerent.ttf", 80);
//        font = cargarFont(Ajustes.ajustesEspecificos.ASSETS + "testmode/disgustin.ttf", 80);
////		font = cargarFont(Ajustes.ajustesEspecificos.ASSETS + "testmode/astonished.ttf", 80);
//
//        LabelStyle style = new LabelStyle(font, Color.TEAL);
//        String nombre = "Consecrated Lands 2";
//        Label label = new Label(nombre, style);
//        label.setPosition(Graficos.getX(0.5f) - font.getBounds(nombre).width / 2, Graficos.getY(0.5f) - font.getBounds(nombre).height / 2);
//        stage.addActor(label);
//
//        LabelStyle style2 = new LabelStyle(font, Color.BLUE);
//        Label label2 = new Label("FREETYPE", style2);
//        label2.setPosition(Graficos.getX(0.5f) - font.getBounds("FREETYPE").width / 2, Graficos.getY(0.2f) - font.getBounds("FREETYPE").height / 2);
//        stage.addActor(label2);
//
//        LabelStyle style3 = new LabelStyle(font, Color.GREEN);
//        Label label3 = new Label("FREETYPE", style3);
//        label3.setPosition(Graficos.getX(0.5f) - font.getBounds("FREETYPE").width / 2, Graficos.getY(0.8f) - font.getBounds("FREETYPE").height / 2);
//        stage.addActor(label3);
//
//        BitmapFont font2 = cargarFont(Ajustes.ajustesEspecificos.ASSETS + "testmode/arial.ttf", 50);
//        TextButtonStyle buttonStyle = (new TextButton("", skin)).getStyle();
//        buttonStyle.font = font;
//        buttonStyle.fontColor = Color.BLACK;
//        TextButton button = new TextButton("FREETYPE", buttonStyle);
//
//        button.setTransform(true);
//        button.setOrigin(Align.center);
//        button.setScale(0.5f);
//
//        button.setPosition(Graficos.getX(0.5f) - button.getWidth() / 2, Graficos.getY(0.03f) - font2.getBounds("FREETYPE").height / 2);
//        stage.addActor(button);
//
//
//    }