package com.badlynx.spaceapps;

import com.badlogic.gdx.Gdx;

public class Graficos {

    public static float getX(float indice) {
        return Gdx.graphics.getWidth() * indice;
    }

    public static float getY(float indice) {
        return Gdx.graphics.getHeight() * indice;
    }

}
